#!/usr/bin/env python3
import argparse
import os
import random
import time
import requests
import shutil
import sys
from datetime import datetime
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from urllib.parse import urlparse, urljoin

# Record the start time when the program starts
app_start_time = time.time()
# List of binary file extensions to skip
binary_extensions = ['.jpg', '.jpeg', '.png', '.gif', '.ico', '.pdf', '.exe', '.svg', '.tiff', '.zip', '.tar.gz', 'tar', '.xz', 'dmg', 
	'mov', 'mp4', 'mp3', 'doc', 'docx', 'xls', 'xlsx']
checked_domains = set()

# Function to remove duplicates from a file
def remove_duplicates_from_file(file_path):
	with open(file_path, "r") as input_file:
		lines = input_file.read().splitlines()

	lines_seen = set()
	deduplicated_lines = []

	for line in lines:
		if line not in lines_seen:
			deduplicated_lines.append(line)
			lines_seen.add(line)

	with open(file_path, "w") as output_file:
		for line in deduplicated_lines:
			output_file.write(line + '\n')

# Function to check the actual URL (for http redirects)
def get_final_url(url):
	headers = {'User-Agent': user_agent}
	try:
		response = requests.head(url, allow_redirects=True, headers=headers, timeout=30)
		return response.url
	except Exception as e:
		print(f"Error getting final URL for {url}: {e}")
		return url  # Return the original URL if error occurs

# Function to check if a link is internal
def is_internal_link(link, domain):
	parsed_link = urlparse(link)
	parsed_domain = urlparse(domain)
	return parsed_link.netloc == parsed_domain.netloc or parsed_link.netloc.endswith("." + parsed_domain.netloc)

def is_binary_link(url):
	for ext in binary_extensions:
		if url.endswith(ext):
			return True
	return False

# Function to extract internal links from a webpage
def extract_internal_links(url, page, visited_links, domain, all_internal_links):
	internal_links = set()
	new_links_found = False

	try:
		soup = BeautifulSoup(page, 'html.parser')
		

		for link in soup.find_all('a', href=True):
			href = link['href']
			full_link = urljoin(url, href)

			# Check if the link is not in the all_internal_links set and not in the database
			if (is_internal_link(full_link, domain) and
				full_link not in visited_links and
				full_link not in all_internal_links and
				full_link not in internal_links and
				not "#" in full_link and
				not is_binary_link(full_link)):

				# Remove the query string and add the base URL to internal_links
				full_link_without_query = full_link.split('?')[0]
				internal_links.add(full_link_without_query)

				# Update the href attribute of the anchor tag with the cleaned full_link
				link['href'] = full_link_without_query

				print(f">> Found link: {full_link_without_query}")  # Print new links
				all_internal_links.add(full_link_without_query)  # Add it to the set first
				new_links_found = True  # Set the flag to True since a new link was found

		# Check if no new links were found
		if not new_links_found:
			print(">> No new links found")

	except Exception as e:
		print(f"Error extracting links from {url}: {e}")

	return internal_links

def extract_external_links(url, page, visited_links, domain, api_token, test_mode, unique_domains):
	external_domains = set()

	try:
		soup = BeautifulSoup(page, 'html.parser')

		for link in soup.find_all('a', href=True):
			href = link['href']
			parsed_link = urlparse(href)

			if parsed_link.scheme in {'http', 'https'}:
				full_link = urljoin(url, href)

				# Check if the link is not in the visited_links set and is external to the domain
				if not is_internal_link(full_link, domain) and full_link not in visited_links:
					parsed_link = urlparse(full_link)
					external_domain = parsed_link.hostname.lower() if parsed_link.hostname else None

					if not external_domain:
						continue

					if external_domain.startswith("www."):
						external_domain = external_domain[4:]  # Remove "www."

					if external_domain not in unique_domains:
						unique_domains.add(external_domain)
						external_domains.add(external_domain)

						# Check the domain status immediately
						if api_token:  # Only if the API token is available
							domain_status = check_domain_status(api_token, external_domain, test_mode)
							print(f">>> New domain found: {external_domain}: {domain_status}")
							time.sleep(1)
						else:
							print("*** No API key provided -- will not check domain status")

	except Exception as e:
		print(f"Error extracting external links from {url}: {e}")

	return external_domains

# Download the page
def fetch_page(url, driver):
	attempts = 0  # Counter for retry attempts

	while attempts < 5:
		try:
			print(f"> Crawling {url}")
			driver.get(url)
			return driver, True

		except Exception as e:
			if "ERR_NAME_NOT_RESOLVED" in str(e):
				print(f"Domain could not be resolved: {url}")
				return driver, False  # Skip this URL and proceed to the next

			print(f"Exception occurred: {e}")
			attempts += 1  # Increment the retry counter
			if "Unable to execute request for an existing session" in str(e):
				print("Reinitializing WebDriver...")
				try:
					driver.quit()  # Try to quit; if this fails, it's okay—see next line
				except:
					pass  # If quit() throws an exception, we're going to reinitialize anyway
				del driver  # Delete the driver object
				driver = None  # Set it to None for good measure
				time.sleep(2)
				driver = load_browser(remote)
				time.sleep(30)
			else:
				print(f"Error fetching {url}: {e}")
				if attempts == 5:  # Stop after 3 failed attempts
					print(f"*** Max attempts reached for URL {url}. Skipping...")
					return driver, False  # Return the set, even if it's empty

	return driver, False

def scan_domain_iterative(start_url, driver, visited_links, to_visit_links, domain, api_token, test_mode, all_internal_links, unique_domains):
	stack = [start_url]
	
	while stack:
		current_url = stack.pop()
		
		if current_url in visited_links:
			print(f">> Skipping {current_url} as it's already visited")
			continue

		if is_binary_link(current_url):
			continue

		visited_links.add(current_url)
		if current_url in to_visit_links:
			to_visit_links.remove(current_url)

		final_url = get_final_url(current_url)
		if not is_internal_link(final_url, domain):
			print(f">> Skipping {final_url} as it's not from the initial domain {domain}")
			continue

		driver, successful_page_fetch = fetch_page(current_url, driver)
		if successful_page_fetch:
			internal_links = extract_internal_links(current_url, driver.page_source, visited_links, domain, all_internal_links)
			external_domains = extract_external_links(current_url, driver.page_source, visited_links, domain, api_token, test_mode, unique_domains)
			to_visit_links.update(internal_links)
		
			# Save to the database file
			save_to_file(internal_links, all_internal_links_file, 'a')
			save_external_domains_to_file(unique_domains)
		
			# Add unvisited internal_links to stack
			for link in internal_links:
				if link not in visited_links:
					stack.append(link)
		
		random_seconds = random.uniform(2, 10)
		current_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
		print(f"\n*** [{current_time}] Sleeping for {random_seconds} seconds\n")
		time.sleep(random_seconds)

def save_to_file(links, filename, mode):
	with open(filename, mode) as file:
		for link in links:
			file.write(f"{link}\n")

def save_external_domains_to_file(unique_domains):
	existing_domains = set()

	# Read existing domains from the file
	if os.path.isfile(domains_file):
		with open(domains_file, "r") as file:
			existing_domains = {line.strip() for line in file if line.strip()}

	# Add the unique external domains to the set
	existing_domains.update(unique_domains)
	sorted_existing_domains = sorted(existing_domains)

	# Write the updated domains back to the file
	with open(domains_file, "w") as file:
		for domain in sorted_existing_domains:
			if domain:  # make sure it's not an empty line or ""
				file.write(domain + '\n')

def check_domain_status(api_token, domain, test_mode):
	# Perform the domain status check
	if test_mode:
		return 'test'

	if domain in checked_domains:
		return

	headers = {
		"Content-Type": "application/json",
		"Authorization": f"Bearer {api_token}",
		"User-Agent": "superuser.one"
	}
	url = f"{transip_api_url}/domain-availability/{domain}"

	retry_count = 0
	wait_time = 30

	while retry_count < 10:
		try:
			response = requests.get(url, headers=headers)
			# Check the response headers for rate limiting information
			x_rate_limit_remaining = response.headers.get('X-Rate-Limit-Remaining')
			x_rate_limit_reset = response.headers.get('X-Rate-Limit-Reset')

			# If TransIP says we're nearing our remaining API calls, just sleep for a bit
			if int(x_rate_limit_remaining) < 100:
				print(f"Rate limit remaining is less than 100. Sleeping for {wait_time} seconds")
				time.sleep(wait_time)
				wait_time *= 2  # Double the wait time for the next iteration
				retry_count += 1
				continue

			if response.status_code == 429:
				current_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
				print(f"*** [{current_time}] Rate limit exceeded. Sleeping for {wait_time} seconds")
				time.sleep(wait_time)
				wait_time *= 2  # Double the wait time for the next iteration
				retry_count += 1
				continue

			if response.status_code == 200:
				status = response.json().get('availability', {}).get('status', 'unknown')

				# Update checked_domains set to remember that we've checked this domain
				checked_domains.add(domain)

				# Check if the status is "free" and write to the file
				if status == "free":
					free_domain = f"{domain},{status}"
					save_to_file([free_domain], available_domains_file, "a")

				return status
			else:
				print(f"*** Unexpected status code {response.status_code}. Retrying")
				retry_count += 1
		except Exception as e:
			print(f"*** Exception while checking {domain}: {e}. Retrying")
			retry_count += 1
			print("*** Sleeping for 5 minutes")
			time.sleep(300)
			continue
	
	current_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
	print(f"*** [{current_time}] Maximum retries reached. Unable to check status for {domain}")
	return 'rate_limited'


def load_browser(remote=None):
	global user_agent
	global chrome_download_dir

	chrome_options = Options()
	chrome_download_dir = '/tmp/chrome-headless'
	os.makedirs(chrome_download_dir, exist_ok=True)
	chrome_options.add_experimental_option('prefs', {
		'download.default_directory': chrome_download_dir
	})
	user_agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36"
	chrome_options.add_argument(f'user-agent={user_agent}')
	chrome_options.add_argument('--headless')
	current_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

	if remote:
		print(f"*** [{current_time}] Initializing Chrome at {remote}\n")
		command_executor = f"http://{remote}:4445/wd/hub"
		try:
			driver = webdriver.Remote(
				command_executor=command_executor,
				options=chrome_options,
			)
		except Exception as e:
			print(f"Failed to initialize Chrome at {command_executor}: {e}")
			sys.exit(1)
	else:
		print("*** [{current_time}] Initializing local Chrome\n")
		service = Service()
		driver = webdriver.Chrome(service=service, options=chrome_options)

	return driver

def test_transip_api(api_key):
	url = f"{transip_api_url}/api-test"
	headers = {
		"Authorization": f"Bearer {api_key}"
	}
	
	try:
		response = requests.get(url, headers=headers)
		if response.status_code == 200:
			return True, ""
		elif response.status_code == 401:
			return False, "*** TransIP API key is invalid."
		else:
			return False, f"*** Received unexpected status code from TransIP: {response.status_code}."
	except Exception as e:
		return False, f"An error occurred: {e}"

def main():
	global all_internal_links_file
	global visited_links_file
	global to_visit_links_file
	global domains_file
	global available_domains_file
	global remote
	global transip_api_url
	transip_api_url = "https://api.transip.nl/v6"

	parser = argparse.ArgumentParser(description="Crawl and find internal links on specified domain(s).")
	parser.add_argument("--domains", nargs='+', required=True, help="One or more domains to crawl")
	parser.add_argument('--api', help='API token for TransIP')
	parser.add_argument('--test', action='store_true', help='Test mode (do not submit API requests)', default=False)
	parser.add_argument('--remote', help='Run Chrome via docker (provide remote host)')
	args = parser.parse_args()

	api_token = args.api
	test_mode = args.test
	remote = args.remote

	if not test_mode:
		api_ok, api_status = test_transip_api(api_token)
		if not api_ok:
			print(api_status)
			print("Exiting now...")
			sys.exit(1)

	# Create the 'db' folder if it doesn't exist
	os.makedirs('db', exist_ok=True)

	# Define file paths
	all_internal_links_file = os.path.join('db', 'links_internal.txt')
	visited_links_file = os.path.join('db', 'links_visited.txt')
	to_visit_links_file = os.path.join('db', 'links_to_visit.txt')
	domains_file = os.path.join('db', 'domains_all.txt')
	available_domains_file = os.path.join('db', 'domains_available.txt')

	# Initialize sets
	visited_links = set()
	to_visit_links = set()
	all_internal_links = set()

	if os.path.isfile(domains_file):
		with open(domains_file, "r") as file:
			unique_domains = {line.strip() for line in file if line.strip()}
	else:
		unique_domains = set()

	# Initialize the driver!
	driver = load_browser(remote)

	# Create or open necessary files
	for file_path in [all_internal_links_file, visited_links_file, to_visit_links_file, domains_file, available_domains_file]:
		if not os.path.isfile(file_path):
			open(file_path, 'a').close()

	# Load visited_links and to_visit_links from respective files
	with open(visited_links_file, "r") as f:
		visited_links = {line.strip() for line in f if line.strip()}
	with open(to_visit_links_file, "r") as f:
		to_visit_links = {line.strip() for line in f if line.strip()}

	for domain in args.domains:
		# Normalize the domain URL
		if not domain.startswith(('http://', 'https://')):
			domain = 'https://' + domain

		# Check if domain is already in visited_links
		if domain in visited_links:
			print(f"*** Continuing crawl for {domain}\n")

			# Resume crawling, skipping already visited pages
			for link in set(to_visit_links):  # Creating a shallow copy to iterate over
				if is_internal_link(link, domain):
					scan_domain_iterative(link, driver, visited_links, to_visit_links, domain, api_token, test_mode, all_internal_links, unique_domains)

		else:
			print(f"*** Starting new crawl for {domain}\n")
			# Start a fresh crawl for this domain
			scan_domain_iterative(domain, driver, visited_links, to_visit_links, domain, api_token, test_mode, all_internal_links, unique_domains)

	# Save visited_links and to_visit_links back to respective files
	with open(visited_links_file, "w") as f:
		for link in visited_links:
			f.write(f"{link}\n")
	with open(to_visit_links_file, "w") as f:
		for link in to_visit_links:
			f.write(f"{link}\n")

	driver.quit()

	# Remove anything Chrome may have downloaded
	shutil.rmtree(chrome_download_dir)

	# Calculate the elapsed time when the program quits
	app_end_time = time.time()
	app_elapsed_time = app_end_time - app_start_time

	# Convert the elapsed time to days, hours, minutes, and seconds
	days, seconds = divmod(app_elapsed_time, 86400)
	hours, seconds = divmod(seconds, 3600)
	minutes, seconds = divmod(seconds, 60)

	# Print the total elapsed time
	current_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
	print(f"*** [{current_time}] Run time: {int(days)} days, {int(hours)} hours, {int(minutes)} minutes, {int(seconds)} seconds")

if __name__ == "__main__":
	main()
