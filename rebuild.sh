#!/bin/bash

git pull

arch=$(uname -m)
BASEIMAGE=`cat Dockerfile | grep FROM | awk '{print $2}'`
docker pull $BASEIMAGE
docker stop deadlink 2> /dev/null
docker rm deadlink 2> /dev/null

if [[ "$arch" == "aarch64" || "$arch" == "arm64" ]]; then
	docker pull seleniarm/standalone-chromium
	docker stop deadlink-chrome
	docker rm deadlink-chrome
	docker run -d --name deadlink-chrome -p 4445:4444 -p 5900:5900 -p 7900:7900 -e SE_NODE_SESSION_TIMEOUT=1200 --shm-size="2g" seleniarm/standalone-chromium:latest 
elif [[ "$arch" == "amd64" ]]; then
	docker pull selenium/standalone-chrome
	docker stop deadlink-chrome
	docker rm deadlink-chrome
	docker run -d --name deadlink-chrome -p 4445:4444 -p 7900:7900 -e SE_NODE_SESSION_TIMEOUT=1200 --shm-size="2g" selenium/standalone-chrome:latest
else
	echo "Unknown architecture: $arch"
	exit 1
fi

docker build -t deadlink .
echo "Containers built and started. Execute ./run.py to get started."
echo ""

# Check if the ./db directory exists before prompting
if [ -d "./db" ]; then
	echo "Automatically wiping database in 30 seconds."
	echo -n "Do you want to wipe the database? (y/n): "
	read -t 30 -n 1 answer

	# Default to 'y' if no input is given within 30 seconds
	if [ -z "$answer" ]; then
		answer="y"
	fi

	# Proceed based on user's decision
	if [ "$answer" = "y" ]; then
		echo ""
		echo "Proceeding with wipe..."
		olddir=/tmp/deadlink/db/$(date +"%Y-%m-%d_%H-%M-%S")
		mkdir -p "$olddir"
		mv ./db/* "$olddir"
		echo "Database wiped by moving it to $olddir."
	else
		echo ""
		echo "Database wipe cancelled."
	fi
fi