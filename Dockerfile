FROM python:slim

ENV DEBIAN_FRONTEND=non-interactive

RUN apt-get update && apt-get install -y gnupg2 wget curl unzip

RUN pip install --no-cache-dir requests selenium beautifulsoup4 argparse

# This is needed to get output from prints
ENV PYTHONUNBUFFERED=1

RUN useradd -ms /bin/bash deadlink

USER deadlink

WORKDIR /home/deadlink

COPY deadlink.py /home/deadlink/deadlink.py

ENTRYPOINT ["python3", "/home/deadlink/deadlink.py"]