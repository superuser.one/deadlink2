# Deadlink2

Replaces the older [Deadlink](https://gitlab.com/superuser.one/deadlink). Over engineered and throwing too many errors.

## Installation

- Clone repo,
- Make sure python3 is installed,
- `pip3 install argparse beautifulsoup4 selenium requests` or `apt install -y python3-bs4 python3-selenium python3-requests`

## Normal Usage

Execute using `./deadlink.py` or `python3 deadlink.py`.

```
$ ./deadlink.py -h
usage: deadlink.py [-h] --domains DOMAINS [DOMAINS ...] [--api API] [--test] [--remote REMOTE]

Crawl and find internal links on specified domain(s).

options:
  -h, --help            show this help message and exit
  --domains DOMAINS [DOMAINS ...]
                        One or more domains to crawl
  --api API             API token for TransIP
  --test                Test mode (do not submit API requests)
  --remote REMOTE       Run Chrome via docker (provide remote host))
```

- `--domains` is one or multiple domains to scan.
- `--api` is the TransIP API. Can be retrieved using [TransIP-Python](https://gitlab.com/superuser.one/transip-python/).
- `--test` will run the program but not actually query TransIP API. Good for testing and not spamming the API. 
- `--remote` is optional Selenium container/address to use (using hardcoded port 4445).

The results will be printed in the terminal, and saved under the `db` folder. 

## Docker Usage

- Be sure to be in the Git project directory as the volume path is `./db`.
- Build the containers using `rebuild.sh`. This will pull either `seleniarm/standalone-chromium` (ARM) or `selenium/standalone-chrome` (amd64) and build the `Dockerfile`.
- Execute the program using `python3 run.py` and be sure to include the same parameters.
- On Linux you may need to `chmod -R 777 db` (or properly chown it) to make it work.

```
$ ./run.py -h
usage: run.py [-h] --domains DOMAINS [DOMAINS ...] [--api API] [--test] [--remote REMOTE]

Run deadlink Docker container with provided arguments.

options:
  -h, --help            show this help message and exit
  --domains DOMAINS [DOMAINS ...]
                        One or more domains to crawl
  --api API             API token for TransIP
  --test                Test mode (do not submit API requests)
  --remote REMOTE       Run Chrome via docker (include remote address, uses port 4445))
```

This will send the container to the background (using `-d` in `docker run`). Check the output/logs using `docker logs -f deadlink`.

## database

The `db` will create three files:

- `internal_links.txt`: all the internal links it found while scanning. Used to browser the entire site,
- `domains.txt`: all external domains it found while scanning for links. Used to check with TransIP the domain status,
- `available_domains.txt`: all external domains that are `free` according to the TransIP API.
