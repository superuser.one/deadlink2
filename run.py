#!/usr/bin/env python3

import argparse
import subprocess
import time
import sys

def main():
	# Initialize argument parser
	parser = argparse.ArgumentParser(description='Run deadlink Docker container with provided arguments.')

	# Add arguments
	parser.add_argument("--domains", nargs='+', required=True, help="One or more domains to crawl")
	parser.add_argument('--api', help='API token for TransIP')
	parser.add_argument('--test', action='store_true', help='Test mode (do not submit API requests)', default=False)
	parser.add_argument('--remote', help='Run Chrome via docker (include remote address, uses port 4445)')

	# Parse arguments
	args = parser.parse_args()

	# Check if 'deadlink:latest' Docker image exists
	check_image_cmd = ["docker", "images", "-q", "deadlink:latest"]
	result = subprocess.run(check_image_cmd, capture_output=True, text=True)

	# If the image is not found, run ./rebuild.sh
	if not result.stdout.strip():
		print("*** 'deadlink:latest' image not found. Running ./rebuild.sh...")
		rebuild_result = subprocess.run(["./rebuild.sh"])
		if rebuild_result.returncode != 0:
			print("*** Error: Failed to rebuild the image.", file=sys.stderr)
			sys.exit(1)

	# Construct docker run command
	docker_cmd = ["docker", "run", "-d", "--rm", "-v", "./db/:/home/deadlink/db", "--name", "deadlink", "deadlink"]

	# Append provided arguments to docker command
	if args.domains:
		docker_cmd.append("--domains")
		docker_cmd.extend(args.domains)
	if args.api:
		docker_cmd.append("--api")
		docker_cmd.append(args.api)
	if args.test:
		docker_cmd.append("--test")
	if args.remote:
		docker_cmd.append("--remote")
		docker_cmd.append(args.remote)

	# Execute docker run command
	subprocess.run(["docker", "stop", "deadlink"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
	print("*** Restarting deadlink-chrome container")
	# Restart deadlink-chrome as it can hang if deadlink didn't exit cleanly
	subprocess.run(["docker", "restart", "deadlink-chrome"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
	time.sleep(30)  # give it 30 seconds to start up nicely
	subprocess.run(docker_cmd)
	print("*** run `docker logs -f deadlink` to get the output")

if __name__ == '__main__':
	main()